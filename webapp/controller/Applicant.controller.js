sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/m/MessageToast'
], function(Controller, History, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Workflow.controller.Applicant", {
		onInit: function() {
			var ctrl = this;
			var sViewName = "FormFDP"; // 未來可改成動態接參數顯示view
			var oContent = ctrl.getView().byId("content");

			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);

			new sap.ui.core.mvc.XMLView({
				viewName: "nnext.iq.Workflow.view." + sViewName
			}).placeAt(oContent);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Workflow", "APPLICANT_SUBMIT", this.onSumit, this);
		},
		onSubmitPress: function() {
			// 通知 EmbeddedView 表單回傳 oData
			sap.ui.getCore().getEventBus().publish("FORM", "APPLICANT_SUBMIT");
		},
		onSumit: function(sChanel, sEvent, oData) {
			var ctrl = this;
			
			// 收到 EmbeddedView 表單回傳 oData, 送單.
			$.post("/Flow7Api/api/fdp/m", oData)
				.done(function(data) {
					$.post("/Flow7Api/api/engine/start", oData)
						.done(function(data) {
							if (data == "") {
								MessageToast.show("Success.");
								ctrl._oRouter.navTo("folder");
							} else {
								MessageToast.show("error:" + data);
							}
						});
				});
		},
		onBackPress: function(oEvent) {
			var ctrl = this;
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
				oRouter.navTo("overview", {}, true);
			}
		}
	});
});