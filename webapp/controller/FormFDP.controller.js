sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast"
], function(Controller, JSONModel, Filter, FilterOperator, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Workflow.controller.FormFDP", {
		onInit: function() {
			var ctrl = this;

			ctrl.getOwnerComponent().getRouter().getRoute("applicant").attachPatternMatched(ctrl._onRouteMatched, ctrl);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("FORM", "APPLICANT_SUBMIT", this.onSumit, this);
		},
		_onRouteMatched: function(oEvent) {
			var ctrl = this;
			
			$.ajax("/Flow7Api/api/fdp")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "fdp");
				});
		},
		changeDepartment: function(oEvent) {
			var ctrl = this;
			var sDepartmentID = oEvent.getParameters().selectedItem.getKey();
			var aFilter = [];

			// build filter array
			aFilter.push(new Filter("departmentId", FilterOperator.Contains, sDepartmentID));

			// filter binding
			var oList = ctrl.getView().byId("Applcant");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		},
		handleSelectDialogPress: function(oEvent) {
			var ctrl = this;

			if (!ctrl._oDialog) {
				ctrl._oDialog = ctrl.getView().byId("testDialog");
				ctrl._oDialog.setModel(ctrl.getView().getModel());
			}

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", ctrl.getView(), ctrl._oDialog);
			ctrl._oDialog.open();
		},
		onSumit: function(oEvent) {
			// 收到 submit 通知, 回傳 oData
			var ctrl = this;
			var oModel = ctrl.getView().getModel("fdp");

			sap.ui.getCore().getEventBus().publish("Workflow", "APPLICANT_SUBMIT", oModel.oData);
		}
	});
});